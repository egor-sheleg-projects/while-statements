﻿namespace WhileStatements
{
    public static class PrimeNumbers
    {
        public static bool IsPrimeNumber(uint n)
        {
            int a = 0;
            int i = 1;
            while (i <= n)
            {
                if (n % i == 0)
                {
                    a++;
                }

                i++;
            }

            if (a == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static uint GetLastPrimeNumber(uint n)
        {
            uint i = 0;
            uint res = 0;

            while (i <= n)
            {
                int a = 0;
                uint j = 1;
                while (j <= i)
                {
                    if (i % j == 0)
                    {
                        a++;
                    }

                    j++;
                }

                if (a == 2)
                {
                    res = i;
                }

                i++;
            }

            return res;
        }

        public static uint SumLastPrimeNumbers(uint n, uint count)
        {
            uint i = n;
            uint res = 0;

            while (i > 0)
            {
                int a = 0;
                uint j = 1;
                while (j <= i && count > 0)
                {
                    if (i % j == 0)
                    {
                        a++;
                    }

                    j++;
                }

                if (a == 2)
                {
                    res += i;
                    count--;
                }

                i--;
            }

            return res;
        }
    }
}
