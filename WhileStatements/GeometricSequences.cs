﻿namespace WhileStatements
{
    public static class GeometricSequences
    {
        public static uint SumGeometricSequenceTerms1(uint a, uint r, uint n)
        {
            uint sum = 0;
            int i = 0;

            while (i < n)
            {
                int j = 0;
                uint rpow = 1;

                while (j < i)
                {
                    rpow *= r;
                    j++;
                }

                sum += a * rpow;
                i++;
            }

            return sum;
        }

        public static uint SumGeometricSequenceTerms2(uint n)
        {
            uint sum = 0;
            int i = 0;

            while (i < n)
            {
                int j = 0;
                uint rpow = 1;

                while (j < i)
                {
                    rpow *= 3;
                    j++;
                }

                sum += 13 * rpow;
                i++;
            }

            return sum;
        }

        public static uint CountGeometricSequenceTerms3(uint a, uint r, uint maxTerm)
        {
            uint sum = a;
            uint i = 0;

            while (sum <= maxTerm)
            {
                sum *= r;
                i++;
            }

            return i;
        }

        public static uint CountGeometricSequenceTerms4(uint a, uint r, uint n, uint minTerm)
        {
            uint sum = a;
            uint i = 0, j = 0;

            while (j < n)
            {
                if (sum >= minTerm)
                {
                    i++;
                }

                sum *= r;
                j++;
            }

            return i;
        }
    }
}
